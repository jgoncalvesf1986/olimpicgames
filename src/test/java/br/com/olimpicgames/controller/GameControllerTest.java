package br.com.olimpicgames.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.time.Month;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.olimpicgames.model.Game;
import br.com.olimpicgames.service.GameService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = GameController.class, secure = false)
public class GameControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	ObjectMapper objectMapper;

	@MockBean
	GameService gameService;
	
	@Test
	public void createGame() throws Exception {	
		
		Game mockGame = new Game("Basquete", "A", LocalDateTime.of(2020, Month.JANUARY, 12, 9, 00, 00),
				LocalDateTime.of(2020, Month.JANUARY, 12, 10, 00, 00), "Brasil v Espanha", "Final");
									
		this.mockMvc.perform(post("/api/game/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(mockGame)))
                .andExpect(status().isCreated());

	}
	
	@Test
	public void createGameValidateStage() throws Exception {
		
		Game mockGameValidateStage = new Game("Basquete", "A", LocalDateTime.of(2020, Month.JANUARY, 01, 8, 00, 00),
				LocalDateTime.of(2020, Month.JANUARY, 01, 9, 00, 00), "Brasil v Espanha", "Eliminatóriass");
		
		this.mockMvc.perform(post("/api/game/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(mockGameValidateStage)))
                .andExpect(status().isPreconditionFailed());

	}
	
	@Test
	public void createGameValidateDuration() throws Exception {
		
		Game mockGameValidateDuration = new Game("Basquete", "A", LocalDateTime.of(2020, Month.JANUARY, 01, 9, 00, 00),
				LocalDateTime.of(2020, Month.JANUARY, 01, 9, 20, 00), "Brasil v Espanha", "Final");
		
		this.mockMvc.perform(post("/api/game/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(mockGameValidateDuration)))
                .andExpect(status().isPreconditionFailed());

	}
		
	@Test
	public void createGameValidateConflict() throws Exception {
		
		Game mockGameConflict = new Game("Basquete", "A", LocalDateTime.of(2020, Month.JANUARY, 10, 9, 00, 00),
				LocalDateTime.of(2020, Month.JANUARY, 10, 10, 00, 00), "Brasil v Espanha", "Final");
		
		this.mockMvc.perform(post("/api/game/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(mockGameConflict)))
                .andExpect(status().isCreated());
		
		this.mockMvc.perform(post("/api/game/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(mockGameConflict)))
                .andExpect(status().isConflict());

	}
	
	@Test
	public void createGameCheckDailyLimit() throws Exception {
		
		Game mockGameCheckDailyLimit = new Game("Basquete", "A", LocalDateTime.of(2020, Month.JANUARY, 22, 9, 00, 00),
				LocalDateTime.of(2020, Month.JANUARY, 22, 10, 00, 00), "Brasil v Espanha", "Final");
		
		this.mockMvc.perform(post("/api/game/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(mockGameCheckDailyLimit)))
                .andExpect(status().isCreated());
		
		mockGameCheckDailyLimit.setStartDate(LocalDateTime.of(2020, Month.JANUARY, 22, 10, 00, 00));
		mockGameCheckDailyLimit.setEndDate(LocalDateTime.of(2020, Month.JANUARY, 22, 11, 00, 00));	
		
		this.mockMvc.perform(post("/api/game/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(mockGameCheckDailyLimit)))
                .andExpect(status().isCreated());
		
		mockGameCheckDailyLimit.setStartDate(LocalDateTime.of(2020, Month.JANUARY, 22, 11, 00, 00));
		mockGameCheckDailyLimit.setEndDate(LocalDateTime.of(2020, Month.JANUARY, 22, 12, 00, 00));	
		
		this.mockMvc.perform(post("/api/game/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(mockGameCheckDailyLimit)))
                .andExpect(status().isCreated());
		
		mockGameCheckDailyLimit.setStartDate(LocalDateTime.of(2020, Month.JANUARY, 22, 12, 00, 00));
		mockGameCheckDailyLimit.setEndDate(LocalDateTime.of(2020, Month.JANUARY, 22, 13, 00, 00));	
		
		this.mockMvc.perform(post("/api/game/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(mockGameCheckDailyLimit)))
                .andExpect(status().isCreated());
		
		mockGameCheckDailyLimit.setStartDate(LocalDateTime.of(2020, Month.JANUARY, 22, 13, 00, 00));
		mockGameCheckDailyLimit.setEndDate(LocalDateTime.of(2020, Month.JANUARY, 22, 14, 00, 00));	
		
		this.mockMvc.perform(post("/api/game/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(mockGameCheckDailyLimit)))
                .andExpect(status().isPreconditionFailed());

	}
	
}

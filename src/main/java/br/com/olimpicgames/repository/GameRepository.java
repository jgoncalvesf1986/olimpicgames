package br.com.olimpicgames.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.olimpicgames.model.Game;

public interface GameRepository extends JpaRepository<Game, Long> {

	@Query("   SELECT COUNT(g) "
			+ "  FROM Game g "
			+ " WHERE g.startDate BETWEEN :startDate AND :endDate "
			+ "   AND LOWER(g.local) = LOWER(:local)")
	Long countByDateTimeBetweenAndLocal(@Param("startDate") LocalDateTime startDate,
			                            @Param("endDate") LocalDateTime endDate, 
			                            @Param("local") String local);
	
	@Query("   SELECT COUNT(g) "
			+ "  FROM Game g "
			+ " WHERE g.startDate BETWEEN :startDate AND :endDate "
			+ "   AND LOWER(g.local) = LOWER(:local) "
			+ "   AND LOWER(g.sport) = LOWER(:sport)")
	Long countByDateTimeBetweenAndLocalAndSport(@Param("startDate") LocalDateTime startDate,
            @Param("endDate") LocalDateTime endDate, 
            @Param("local") String local, 
            @Param("sport") String sport);
	
	
	@Query("   SELECT g "
			+ "  FROM Game g "
			+ " WHERE UPPER(:stage) = 'N' OR LOWER(g.stage) = LOWER(:stage) "
			+ "   ORDER BY g.startDate ")
	List<Game> findGamesByStage(@Param("stage") String stage);

}

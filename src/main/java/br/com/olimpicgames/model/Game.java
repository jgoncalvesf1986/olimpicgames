package br.com.olimpicgames.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters.LocalDateTimeConverter;

@Entity
public class Game implements Serializable{
	
	private static final long serialVersionUID = 7937309172899733981L;
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String sport;
	
	private String local;
	
	@Convert(converter = LocalDateTimeConverter.class)
	private LocalDateTime startDate;
	
	@Convert(converter = LocalDateTimeConverter.class)
	private LocalDateTime endDate;	
	
	private String match;	
	
	private String stage;
		
	public Game() {
		super();
	}
	
	public Game(String sport, String local, LocalDateTime startDate, LocalDateTime endDate, String match,
			String stage) {
		super();
		this.sport = sport;
		this.local = local;
		this.startDate = startDate;
		this.endDate = endDate;
		this.match = match;
		this.stage = stage;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSport() {
		return sport;
	}
	public void setSport(String sport) {
		this.sport = sport;
	}
	public String getLocal() {
		return local;
	}
	public void setLocal(String local) {
		this.local = local;
	}
	public LocalDateTime getStartDate() {
		return startDate;
	}
	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}
	public LocalDateTime getEndDate() {
		return endDate;
	}
	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}
	public String getMatch() {
		return match;
	}
	public void setMatch(String match) {
		this.match = match;
	}
	public String getStage() {
		return stage;
	}
	public void setStage(String stage) {
		this.stage = stage;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Game other = (Game) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Game [id=" + id + ", sport=" + sport + ", local=" + local + ", startDate=" + startDate + ", endDate="
				+ endDate + ", match=" + match + ", stage=" + stage + "]";
	}
	
	
	
	
	
	
	

}

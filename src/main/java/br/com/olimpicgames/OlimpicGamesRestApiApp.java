package br.com.olimpicgames;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"br.com.olimpicgames"})
public class OlimpicGamesRestApiApp {

	public static void main(String[] args) {
		SpringApplication.run(OlimpicGamesRestApiApp.class, args);
	}

}

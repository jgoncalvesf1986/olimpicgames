package br.com.olimpicgames.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.olimpicgames.model.Game;
import br.com.olimpicgames.service.GameService;
import br.com.olimpicgames.util.CustomErrorType;

@RestController
@RequestMapping("/api")
public class GameController {

	public static final Logger logger = LoggerFactory.getLogger(GameController.class);

	@Autowired
	GameService gameService;

	@RequestMapping(value = "/game/", method = RequestMethod.GET)
	public ResponseEntity<List<Game>> listAllGames() {		
		List<Game> games = gameService.findGamesByStage("N"); 
		if (games != null && games.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Game>>(games, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/game/{stage}", method = RequestMethod.GET)
	public ResponseEntity<List<Game>> findGamesByStage(@PathVariable String stage) {
		stage = (stage.isEmpty() ? "N":stage); 
		List<Game> games = gameService.findGamesByStage(stage);
		if (games != null && games.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Game>>(games, HttpStatus.OK);
	}

	@RequestMapping(value = "/game/", method = RequestMethod.POST)
	public ResponseEntity<?> createGame(@RequestBody Game game, UriComponentsBuilder ucBuilder) {
		
		logger.info("Creating Game : {}", game); 

		if (!gameService.validateStage(game)) {
			logger.error(
					Messages.getString("GameController.3")); 
			return new ResponseEntity(new CustomErrorType(
					Messages.getString("GameController.4")), 
					HttpStatus.PRECONDITION_FAILED);
		}

		if (gameService.isGameExist(game)) {
			logger.error(
					Messages.getString("GameController.5")); 
			return new ResponseEntity(new CustomErrorType(
					Messages.getString("GameController.6")), 
					HttpStatus.CONFLICT);
		}

		if (!gameService.validateDuration(game)) {
			logger.error(
					Messages.getString("GameController.7")); 
			return new ResponseEntity(new CustomErrorType(Messages.getString("GameController.8")), 
					HttpStatus.PRECONDITION_FAILED);
		}

		if (!gameService.checkDailyLimit(game)) {
			logger.error(
					Messages.getString("GameController.9")); 
			return new ResponseEntity(
					new CustomErrorType(Messages.getString("GameController.10")), 
					HttpStatus.PRECONDITION_FAILED);
		}

		gameService.addGame(game);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/api/game/{id}").buildAndExpand(game.getId()).toUri());  
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	

}

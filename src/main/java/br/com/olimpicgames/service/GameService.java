package br.com.olimpicgames.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.olimpicgames.model.Game;
import br.com.olimpicgames.repository.GameRepository;

@Service
public class GameService {

	@Autowired
	GameRepository gameRepository;

	public List<Game> getAllGames() {
		List<Game> games = new ArrayList<Game>();
		gameRepository.findAll().forEach(games::add);
		return games;
	}
	
	public List<Game> findGamesByStage(String stage) {
		List<Game> games = new ArrayList<Game>();
		gameRepository.findGamesByStage(stage).forEach(games::add);
		return games;
	}	

	public Game addGame(Game game) {
		return gameRepository.save(game);
	}
	

	public boolean isGameExist(Game game) {

		Long count = gameRepository.countByDateTimeBetweenAndLocalAndSport(game.getStartDate(), game.getEndDate(), game.getLocal(), game.getSport());
		
		if (count > 0) {
			return true;
		} else {
			return false;
		}

	}
		
    public boolean checkDailyLimit(Game game) {
    	
    	LocalDateTime startDate = game.getStartDate().toLocalDate().atTime(0, 0);
    	LocalDateTime endDate = startDate.plusDays(1).toLocalDate().atTime(0, 0);
		
    	Long count = gameRepository.countByDateTimeBetweenAndLocal(startDate, endDate, game.getLocal());
		

		if (count < 4) {
			return true;
		} else {
			return false;
		}

	}
    
    public boolean validateStage(Game game) {

		List<String> stageList = Arrays.asList("eliminatórias", "oitavas de final", "quartas de final", "semifinal,", "final");

		if (stageList.contains(game.getStage().toLowerCase())) {
			return true;
		} else {
			return false;
		}
	}

    public boolean validateDuration(Game game) {

		Duration duration = Duration.between(game.getStartDate(), game.getEndDate());
		long minuts = duration.toMinutes();

		if (minuts >= 30) {
			return true;
		} else {
			return false;
		}

	}

}

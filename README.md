## Entrega Desafio Java [Hands On] ##

A proposta do desafio é a criação de uma API RESTful, para gerir dados das competições dos
Jogos Olímpicos Tokyo 2020. 

### Tecnologias Usadas ###
As tecnologias usadas foram: - Java, Spring Boot, Spring Data, Apache Maven, HsqlDB, Jackson, Hibernate e JPA.

### Motivação ###
O que me motiva é a busca por novos desafios, tanto na vida profissional quanto pessoal, me motiva​ poder contribuir com minha experiência, alcançar ou superar metas desafiadoras​.​

### Observação ###
A versão do java deve ser 1.8.